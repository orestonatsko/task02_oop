package store;

import store.Item.StoreItem;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class SalesManager {

    List<StoreItem> findGoodsByMaxPrice(List<StoreItem> goods, int price) {
        List<StoreItem> items = goods.stream().filter(item -> item.getPrice() < price).collect(Collectors.toList());
        sortByPrice(items);
        return items;

    }

    List<StoreItem> findGoodsByType(List<StoreItem> goods, String type) {
        List<StoreItem> items = goods.stream().filter(item -> item.getType().equals(type)).collect(Collectors.toList());
        sortByType(items);
        return items;

    }

    List<StoreItem> findGoodsByPriceAndType(List<StoreItem> goods, int price, String type, int sort) {
        List<StoreItem> items = goods.stream().filter(item -> item.getPrice() < price && item.getType().equals(type)).collect(Collectors.toList());
        sortByPrice(items);
        if (sort == 1) {
            sortByPrice(items);
        } else {
            sortByType(items);
        }
        return items;

    }

    void showGoods(List<StoreItem> items) {
        items.forEach(System.out::println);
    }

    private void sortByPrice(List<StoreItem> items) {
        items.sort(Comparator.comparingInt(StoreItem::getPrice).reversed());
    }

    private void sortByType(List<StoreItem> items) {
        items.sort(Comparator.comparing(StoreItem::getType));
        items.sort(Comparator.comparing(StoreItem::getName));
    }

    StoreItem sell(List<StoreItem> goods, int id) {
        StoreItem soldItem = null;
        for (int i = 0; i < goods.size(); i++) {
            if (goods.get(i).getId() == id) {
                soldItem = goods.remove(i);
            }
        }
        return soldItem;
    }

     StoreItem findGoodsById(List<StoreItem> items, int id) {
        StoreItem item = null;
        for (StoreItem i : items) {
            if (i.getId() == id) {
                item = i;
            }
        }
        return item;
    }
}
