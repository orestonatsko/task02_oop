package store;

import store.Item.StoreItem;
import store.generators.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

class Courier {
    private ItemGenerator itemGenerator;

    Courier(ItemGenerator ig) {
        this.itemGenerator = ig;
    }

    final List<StoreItem> deliver() {
        List<StoreItem> items = new ArrayList<>();
        generate(items);
        Collections.shuffle(items);
        return items;
    }

    private void generate(List<StoreItem> items) {
        Random random = new Random();
        int max = 15;
        int min = 7;
        int quantity = random.nextInt((max - min) + 1) + min;
        for (int i = 0; i < quantity; i++) {
            items.add(itemGenerator.generateItem());
        }
    }


}

