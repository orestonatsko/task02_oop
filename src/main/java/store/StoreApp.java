package store;

import store.generators.*;

class StoreApp {

    public static void main(String[] args) {
        Store store = new Store();
        store.receiveDelivery(new Courier(new BowlGenerator()).deliver());
        store.receiveDelivery(new Courier(new DoorGenerator()).deliver());
        store.receiveDelivery(new Courier(new FaucetGenerator()).deliver());
        store.receiveDelivery(new Courier(new LaminateFlooringGenerator()).deliver());
        store.receiveDelivery(new Courier(new PaintGenerator()).deliver());
        store.receiveDelivery(new Courier(new VarnishGenerator()).deliver());
        store.hireManager(new SalesManager());
        store.open();
    }
}
