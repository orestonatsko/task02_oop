package store.Item;

import store.ItemType;

public class Paint extends StoreItem {
    private String color;

    public Paint(String name, ItemType type, int price, String c) {
        super(name, type, price);
        this.color = c;
    }

    @Override
    public final String toString() {
        return getName() + "- id: " + getId() +" color: "  + color + "; price: " + getPrice() + "$";
    }
}
