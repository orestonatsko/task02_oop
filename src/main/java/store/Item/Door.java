package store.Item;

import store.ItemType;

public class Door extends StoreItem {
    private int height;
    private double width;

    public Door(String name, ItemType type, int price, int h, double w) {
        super(name, type, price);
        this.height = h;
        this.width = w;
    }

    @Override
    public final String toString() {
        return getName() + "- id: " + getId() + " height: " + height + "; width: " + width + "; price: " + getPrice() + "$";
    }
}
