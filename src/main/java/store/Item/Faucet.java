package store.Item;

import store.ItemType;

public class Faucet extends StoreItem {
    private String coating;

    public Faucet(String name, ItemType type, int price, String c) {
        super(name, type, price);
        this.coating = c;
    }

    @Override
    public final String toString() {
        return getName() + "- id: " + getId() + " coating: " + coating + "; price: " + getPrice() + "$";
    }
}
