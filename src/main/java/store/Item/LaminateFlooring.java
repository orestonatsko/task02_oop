package store.Item;


import store.ItemType;

public class LaminateFlooring extends StoreItem {
    private String color;

    public LaminateFlooring(String name, ItemType type, int price, String c) {
        super(name, type, price);
        this.color = c;

    }

    @Override
    public final String toString() {
        return getName() + "- id: " + getId() + " color: " + color  + "; price: " + getPrice() + "$" ;
    }
}
