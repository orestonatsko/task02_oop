package store.Item;

import store.ItemType;

public abstract class StoreItem {
    private static int count = 0;
    private int id;
    private String name;
    private ItemType type;
    private int price;

    public StoreItem(String n, ItemType t, int p) {
        id = ++count;
        this.name = n;
        this.type = t;
        this.price = p;
    }

    public final String getType() {
        return type.toString();
    }

    public final int getPrice() {
        return price;
    }

    public final String getName() {
        return name;
    }

    public final int getId() {
        return id;
    }


}
