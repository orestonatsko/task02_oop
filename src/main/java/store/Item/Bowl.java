package store.Item;

import store.ItemType;

public class Bowl extends StoreItem {
    private boolean seat;
    private int weight;

    public Bowl(String name, ItemType type, int price, boolean s, int w) {
        super(name, type, price);
        this.seat = s;
        this.weight = w;
    }

    @Override
    public final String toString() {
        return  getName() + "- id: " + getId()+ "  seat: " + ((seat) ? "yes" : "no") + ";" + " weight: " + weight + "; price: " + getPrice() + "$";
    }
}
