package store.Item;

import store.ItemType;

public class Varnish extends StoreItem {
    private String kind;

    public Varnish(String name, ItemType type, int price, String k) {
        super(name, type, price);
        this.kind = k;
    }

    @Override
    public final String toString() {
        return getName() + "- id: " + getId() + " type: " + kind + "; price: " + getPrice() + "$";
    }
}
