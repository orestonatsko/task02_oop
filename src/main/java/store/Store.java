package store;

import store.Item.StoreItem;

import java.util.ArrayList;
import java.util.List;

class Store {
    private SalesManager sManager;
    private List<StoreItem> goods;
    private StoreMenu menu;
    private boolean storeIsOpen = false;


    Store() {
        menu = new StoreMenu();
    }

    void receiveDelivery(List<StoreItem> delivery) {
        if (goods == null) {
            goods = new ArrayList<>();
        }
        goods.addAll(delivery);
    }

    void hireManager(SalesManager salesManager) {
        sManager = salesManager;
    }

    void open() {
        storeIsOpen = true;
        while (storeIsOpen) {
            menu.show();
        }
    }


    class StoreMenu {
        private int userChoice;
        private InputHandler inputHandler;

        StoreMenu() {
            inputHandler = new InputHandler();
        }


        void show() {
            System.out.println("************************");
            System.out.println("BUILDING MATERIALS STORE");
            System.out.println("************************");
            showMainMenu();

        }

        private void showBuyMenu() {
            int exit = -1;
            List<StoreItem> items;
            System.out.println("What do you want to buy?");
            showMenuByType();
            String type = inputHandler.convertInputToItemType(inputHandler.takeInput(3));
            showMenuByPrice();
            int price = inputHandler.takeInput();
            items = sManager.findGoodsByPriceAndType(goods, price, type, 2);
            if (items.isEmpty()) {
                error();
            } else {
                sManager.showGoods(items);
                System.out.println("Chose item id  from slot above or enter " + exit + " to exit:");
                StoreItem soldItem = null;
                do {
                    userChoice = inputHandler.takeInput();
                    for (StoreItem item : items) {
                        if (item.getId() == userChoice) {
                            soldItem = sManager.sell(goods, userChoice);
                            System.out.println("You have bought " + soldItem + "!");
                            System.out.println("Congratulations!!!");
                        }
                    }
                    items.remove(soldItem);
                } while (userChoice != exit);
            }
        }

        private void showFindGoodsMenu() {
            System.out.println("In what way do you want to find the good's?");
            System.out.println("1 - by price");
            System.out.println("2 - by type");
            System.out.println("3 - by price and type");
            System.out.println("4 - by id");
            System.out.println("5 - return to previous menu");
            userChoice = inputHandler.takeInput(5);
            List<StoreItem> items;
            switch (userChoice) {
                case 1:
                    showMenuByPrice();
                    items = sManager.findGoodsByMaxPrice(goods, inputHandler.takeInput());
                    if (!items.isEmpty()) {
                        System.out.println("Available goods:");
                        sManager.showGoods(items);
                    } else {
                        System.out.println("Sorry, No good's at the specified price");
                        System.out.println();
                    }
                    break;
                case 2:
                    showMenuByType();
                    items = sManager.findGoodsByType(goods, inputHandler.convertInputToItemType(inputHandler.takeInput(3)));
                    sManager.showGoods(items);
                    break;
                case 3:
                    showMenuByPrice();
                    int price = inputHandler.takeInput();
                    showMenuByType();
                    String type = inputHandler.convertInputToItemType(inputHandler.takeInput(3));
                    showSortModeMenu();
                    int sortMode = inputHandler.takeInput(2);
                    items = sManager.findGoodsByPriceAndType(goods, price, type, sortMode);
                    if (!items.isEmpty()) {
                        sManager.showGoods(items);
                    } else {
                        error();
                    }
                    break;
                case 4:
                    System.out.println("Enter product id:");
                    int id = inputHandler.takeInput();
                    StoreItem item = sManager.findGoodsById(goods, id);
                    if (item != null) {
                        System.out.println(item);
                    } else {
                        error();
                    }
                    break;
            }
        }

        private void error() {
            System.out.println("Sorry, No good's at the specified");
        }


        private void showSortModeMenu() {
            System.out.println("Sort mode:");
            System.out.println("1 - sort by price");
            System.out.println("2 - sort by type");
        }

        private void showMenuByPrice() {
            System.out.println("Enter a maximum price:");
        }

        private void showMenuByType() {
            System.out.println("Enter a type:");
            System.out.println("1 - plumbing");
            System.out.println("2 - wood materials");
            System.out.println("3 - paint varnishes");
        }

        void showMainMenu() {
            System.out.println("Please, choose:");
            System.out.println("1 - find good's");
            System.out.println("2 - buy good's");
            System.out.println("3 - exit");
            userChoice = inputHandler.takeInput(3);
            switch (userChoice) {
                case 1:
                    showFindGoodsMenu();
                    break;
                case 2:
                    showBuyMenu();
                    break;
                case 3:
                    storeIsOpen = false;
                    System.out.println("Exit...Good Bye!");
                    break;
            }


        }
    }
}
