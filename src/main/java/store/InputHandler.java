package store;

import java.util.InputMismatchException;
import java.util.Scanner;

class InputHandler {

    int takeInput(int menuItems) {
        int input = 0;
        do {
            System.out.println("Your choice:");
            Scanner scanner = new Scanner(System.in);
            try {
                input = scanner.nextInt();
                if (input < 1 || input > menuItems) {
                    error();
                }
            } catch (InputMismatchException e) {
                error();
            }
        } while (input < 1 || input > menuItems);
        return input;
    }

    int takeInput() {
        int input = 0;
        while (input == 0) {
            System.out.println("Your choice:");
            Scanner scanner = new Scanner(System.in);
            try {
                input = scanner.nextInt();
            } catch (InputMismatchException e) {
                error();
            }
        }
        return input;
    }
     String convertInputToItemType(int input) {
        switch (input) {
            case 1:
                return "PLUMBING";
            case 2:
                return "WOOD_MATERIALS";
            case 3:
                return "PAINTS_VARNISHES";
            default:
                return "NONE";

        }
    }

    private void error() {
        System.out.println("INCORRECT INPUT!");
    }
}
