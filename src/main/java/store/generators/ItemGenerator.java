package store.generators;

import store.Item.StoreItem;

import java.util.Random;

public interface ItemGenerator {
   StoreItem generateItem();
     int randomPrice();
   default int randomValue(int min, int max){
       Random r = new Random();
       return r.nextInt((max - min) + 1) + min;
   }
}
