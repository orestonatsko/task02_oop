package store.generators;

import store.Item.Paint;
import store.Item.StoreItem;
import store.ItemType;

public class PaintGenerator implements ItemGenerator {
    private String[] colors = {"Gray", "Blue", "Yellow", "Orange", "Black"};
    @Override
    public final StoreItem generateItem() {
        return new Paint("Bowl", ItemType.PAINTS_VARNISHES, randomPrice(), colors[randomColor()]);
    }

    @Override
    public final int randomPrice() {
        int maxP = 300;
        int minP = 20;
        return randomValue(minP, maxP);
    }

    private int randomColor() {
        int maxW = 3;
        int minW = 0;
        return randomValue(minW, maxW);
    }
}
