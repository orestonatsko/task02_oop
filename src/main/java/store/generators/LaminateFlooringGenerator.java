package store.generators;

import store.Item.LaminateFlooring;
import store.Item.StoreItem;
import store.ItemType;

public class LaminateFlooringGenerator implements ItemGenerator {
    private String[] colors = {"Gray", "Brown", "White"};

    @Override
    public final StoreItem generateItem() {
        return new LaminateFlooring("LaminateFlooring", ItemType.WOOD_MATERIALS, randomPrice(), colors[randomColor()]);
    }

    @Override
    public final int randomPrice() {
        int maxP = 700;
        int minP = 20;
        return randomValue(minP, maxP);
    }

    private int randomColor() {
        int maxW = 2;
        int minW = 0;
        return randomValue(minW, maxW);
    }
}
