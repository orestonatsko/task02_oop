package store.generators;

import store.Item.StoreItem;
import store.Item.Varnish;
import store.ItemType;

public class VarnishGenerator implements ItemGenerator {
    private String[] type = {"Glossy", "Opaque"};
    @Override
    public final StoreItem generateItem() {
        return new Varnish("Varnish", ItemType.PAINTS_VARNISHES, randomPrice(), type[randomType()]);
    }

    @Override
    public final int randomPrice() {
        int maxP = 300;
        int minP = 20;
        return randomValue(minP, maxP);
    }

    private int randomType() {
        int minW = 0;
        int maxW = 1;
        return randomValue(minW, maxW);
    }
}
