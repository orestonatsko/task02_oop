package store.generators;

import store.Item.Faucet;
import store.Item.StoreItem;
import store.ItemType;

public class FaucetGenerator  implements  ItemGenerator{
    private String[] coating = {"Alloyed steel", "Plastic", "Brass"};
    @Override
    public final StoreItem generateItem() {
        return new Faucet("Faucet", ItemType.PLUMBING, randomPrice(), coating[randomCoating()]);
    }

    @Override
    public final int randomPrice() {
        int maxP = 1000;
        int minP = 200;
        return randomValue(minP, maxP);
    }

    private int randomCoating() {
        return randomValue(0, 2);
    }

}
