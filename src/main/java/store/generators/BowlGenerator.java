package store.generators;

import store.Item.Bowl;
import store.Item.StoreItem;
import store.ItemType;


public class BowlGenerator implements ItemGenerator {

    @Override
    public final StoreItem generateItem() {
        return new Bowl("Bowl", ItemType.PLUMBING, randomPrice(), randomSeat(), randomWeight());
    }

    @Override
    public final int randomPrice() {
        int maxP = 1000;
        int minP = 100;
        return randomValue(minP, maxP);
    }

    private boolean randomSeat() {
        return Math.random() < 0.5;
    }

    private int randomWeight() {
        int maxW = 12;
        int minW = 5;
        return randomValue(minW, maxW);
    }

}
