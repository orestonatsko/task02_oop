package store.generators;

import store.Item.Door;
import store.Item.StoreItem;
import store.ItemType;


public class DoorGenerator implements ItemGenerator {

    @Override
    public final StoreItem generateItem() {
            return new Door("Door", ItemType.WOOD_MATERIALS, randomPrice(), randomHeight(), 0.5);
    }

    private int randomHeight() {
        int maxH = 5;
        int minH = 2;
        return randomValue(minH, maxH);
    }

    @Override
    public final int randomPrice() {
        int maxP = 3000;
        int minP = 300;
        return randomValue(minP, maxP);
    }


}
